import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';
import * as _ from 'lodash';

import { CatsQuery } from 'src/app/shared/state/cats.query';
import { CatsService } from 'src/app/shared/state/cats.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss'],
})
export class ImageListComponent implements OnInit {
  @ViewChild('myinfinite', { static: true })
  infiniteScroll: IonInfiniteScroll;
  breedSelected;
  loading$ = this.catQuery.selectLoading();
  cat$ = this.catQuery.selectAll();
  page = 0;
  perpage = 10;
  totalPage = 0;
  subscriptions = [];
  meta = null;
  breedId;

  constructor(
    private catQuery: CatsQuery,
    private catService: CatsService,
    private router: Router,
  ) { }
  // ============================================================
  // UTILITIES
  // ============================================================

  // ============================================================
  // VIEW METHODS
  // ============================================================

  doRefresh(event) {

    this.load({});

    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  async load(params: any) {

      if (this.breedId && this.breedSelected) {

      _.set(params, 'breed_ids', this.breedId);


      await this.catService.getImageListPaginated(params).toPromise();
    }



  }
  goToDetails(id){
    this.router.navigate(['details', id], { replaceUrl: true });
}


  // ============================================================
  // ANGULAR LIFECYCLES
  // ============================================================

  ngOnInit() {
    this.breedSelected = undefined;

  }

}
