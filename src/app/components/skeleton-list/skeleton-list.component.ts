import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-skeleton-list',
  templateUrl: './skeleton-list.component.html',
  styleUrls: ['./skeleton-list.component.scss'],
})
export class SkeletonListComponent implements OnInit {

  @Input() count;
  @Input() show = false;
  items = [];

  constructor() { }

  // ============================================================
  // UTILITIES
  // ============================================================

  // ============================================================
  // VIEW METHODS
  // ============================================================

  // ============================================================
  // IONIC/ANGULAR LIFECYCLES
  // ============================================================

  ngOnInit() {
    this.items = Array(this.count).fill(0).map((nr) => nr);
  }

}
