import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ImageListComponent } from './image-list/image-list.component';
import { SkeletonListComponent } from './skeleton-list/skeleton-list.component';
import { ListFooterComponent } from './list-footer/list-footer.component';
const COMPONENTS =
  [
    ImageListComponent,
    SkeletonListComponent,
    ListFooterComponent,
  ];


@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    IonicModule,
  ]
})
export class ComponentsModule { }
