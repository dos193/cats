import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { BreedStore, BreedState } from './breed.store';

@Injectable({ providedIn: 'root' })
export class BreedQuery extends QueryEntity<BreedState> {

  constructor(protected store: BreedStore) {
    super(store);
  }

}
