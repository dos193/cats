import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ToastService } from '../services/toast.service';
import { getApiErrorMessagesFlat } from '../utils/form.utils';
import { CatsStore } from './cats.store';

@Injectable({ providedIn: 'root' })
export class CatsService {

  constructor(
    private catsStore: CatsStore,
    private http: HttpClient,
    private toast: ToastService) {
  }


  getImageListPaginated(option?: any) {
    this.catsStore.setLoading(true);
    return this.http.get<any>(`${environment.api.basePath}/images/search`, { params: option as any }).pipe(
      tap(entities => {
        const data = entities;
        if (data.length > 0){
          this.catsStore.upsertMany(data);
        }else{
          this.catsStore.reset();
        }

        this.catsStore.setLoading(false);
      }),
      catchError(async error => {
        await this.toast.show({
          message:  _.join(getApiErrorMessagesFlat(error), '\n'),
          duration: 3000,
          position: 'top'
        });
        this.catsStore.setLoading(false);
      })

    );
  }
  getById(id){

    return this.http.get<any>(`${environment.api.basePath}/images/${id}`).pipe(

      tap(entity => {
        this.catsStore.add(entity);
      }),
      catchError( async err => {

        await this.toast.show({
          message:  _.join(getApiErrorMessagesFlat(err), '\n'),
          duration: 3000,
          position: 'top'
        });


      })
      );
  }


}
