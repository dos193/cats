import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Breed } from './breed.model';

export interface BreedState extends EntityState<Breed> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'breed' })
export class BreedStore extends EntityStore<BreedState> {

  constructor() {
    super();
  }

}
