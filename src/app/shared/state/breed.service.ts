import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Breed } from './breed.model';
import { BreedStore } from './breed.store';

@Injectable({ providedIn: 'root' })
export class BreedService {

  constructor(private breedStore: BreedStore, private http: HttpClient) {
  }


  getAll() {
    this.breedStore.setLoading(true);
    return this.http.get<any>(`${environment.api.basePath}/breeds`).pipe(tap(entities => {
      const data = entities;
      this.breedStore.set(data);
      this.breedStore.setLoading(false);
    }));

  }
}
