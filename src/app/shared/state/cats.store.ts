import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Cat } from './cat.model';

export interface CatsState extends EntityState<Cat> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'cats' })
export class CatsStore extends EntityStore<CatsState> {

  constructor() {
    super();
  }

}
