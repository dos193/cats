import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

export function mapApiErrorsToForm(errorResponse: any, form: FormGroup) {
  const apiErrors = getApiErrorMessages(errorResponse);

  _.each(apiErrors, (errors, key) => {
    if (form.get(key)) {
      form.get(key).setErrors({
        api: _.join(errors, '\n')
      });
    }
  });
}

export function getApiErrorMessages(errorResponse) {
  const error = _.get(errorResponse, 'error');
  if (_.get(error, 'errors')){
    return (_.get(error, 'errors'));

  } else {
    const err = errorResponse.error.message;
    return {err};
  }
}

export function getApiErrorMessagesFlat(errorResponse) {
  const apiErrors = getApiErrorMessages(errorResponse);

  return _.flatten(_.map(apiErrors, (errors) => {
    return errors;
  }));
}

export function markAllDirty(control: any) {
  if (control.hasOwnProperty('controls')) {
    control.markAsDirty();
    control.updateValueAndValidity();
    const ctrl = control as any;
    // tslint:disable-next-line
    for (let inner in ctrl.controls) {
      markAllDirty(ctrl.controls[inner] as AbstractControl);
    }
  } else {
    (control as FormControl).markAsDirty();
    (control as FormControl).updateValueAndValidity();
  }
}
