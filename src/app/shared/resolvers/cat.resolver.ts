import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CatsQuery } from '../state/cats.query';
import { CatsService } from '../state/cats.service';


@Injectable({
  providedIn: 'root'
})
export class CatResolverService implements Resolve<any> {

  constructor(
    private catQuery: CatsQuery,
    private catService: CatsService
  ) { }
  async resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;
    const checkCat = await this.catQuery.getEntity(id);
    if (!checkCat) {
      await  this.catService.getById(id).toPromise();
    }

    const cat = await this.catQuery.getEntity(id);
    return  cat;

  }
}
