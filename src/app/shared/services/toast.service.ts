import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    public toastCtrl: ToastController,
  ) { }

  async show(options: ToastOptions) {
    const toast = await this.toastCtrl.create(options);
    toast.present();
  }
}
