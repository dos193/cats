import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable()
export class CatInterceptor implements HttpInterceptor {

  constructor(

  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {

    const apiKey = environment.api.apiKey;
    const authReq = request.clone({
      setHeaders: { authority: 'api.thecatapi.com' }
    }
    );
    return next.handle(authReq);
  }
}
