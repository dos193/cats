import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatsQuery } from 'src/app/shared/state/cats.query';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.page.html',
  styleUrls: ['./cat.page.scss'],
})
export class CatPage implements OnInit {
  subscriptions = [];
  id;
  loading = false;
  detail;
  constructor(
    private catQuery: CatsQuery,
    private route: ActivatedRoute,
  ) { }
  // ============================================================
  // UTILITIES
  // ============================================================

  // ============================================================
  // VIEW METHODS
  // ============================================================

  // ============================================================
  // ANGULAR LIFECYCLES
  // ============================================================

  ngOnInit() {

    this.subscriptions.push(this.route.params.subscribe(async params => {
      this.loading = true;
      this.id = params.id;
      if (await this.catQuery.getEntity(this.id)) {

        this.detail = await this.catQuery.getEntity(this.id);

      }
      this.loading = false;


    }));
  }

}
