import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ImageListComponent } from 'src/app/components/image-list/image-list.component';
import { BreedQuery } from 'src/app/shared/state/breed.query';
import { BreedService } from 'src/app/shared/state/breed.service';
import { CatsStore } from 'src/app/shared/state/cats.store';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('imageList', { static: false })
  imageList: ImageListComponent;
  breedId;
  breed$ = this.breedQuery.selectAll();
  constructor(
    private breedQuery: BreedQuery,
    private breedService: BreedService,
    private catStore: CatsStore,

  ) { }
  // ============================================================
  // UTILITIES
  // ============================================================

  // ============================================================
  // VIEW METHODS
  // ============================================================
  async loadBreed() {
    await this.breedService.getAll().toPromise();
  }
  doRefresh(event) {
    this.imageList.doRefresh(event);
  }

  async search() {
    this.catStore.reset();
    this.imageList.breedSelected = true;
    this.imageList.breedId = this.breedId;
    this.imageList.page = 0;
    await this.imageList.load({ limit: 25 });
  }

  // ============================================================
  // ANGULAR LIFECYCLES
  // ============================================================

  async ngOnInit() {
    await this.loadBreed();
  }

}
