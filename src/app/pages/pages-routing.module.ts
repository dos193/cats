import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutPage } from '../layouts/main-layout/main-layout.page';
import { CatResolverService } from '../shared/resolvers/cat.resolver';
import { CatPage } from './cat/cat.page';
import { HomePage } from './home/home.page';

const routes: Routes = [{
  path: '',
  component: MainLayoutPage,
  children:
    [
      {
        path: 'home',
        component: HomePage,
      },
      {
        path: 'details/:id',
        component: CatPage,
        resolve: {
          cat: CatResolverService
        }
      },
    ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
