import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PagesRoutingModule } from './pages-routing.module';
import { ComponentsModule } from '../components/components.module';
import { HomePage } from './home/home.page';
import { CatPage } from './cat/cat.page';

const PAGES = [HomePage, CatPage];

@NgModule({
  declarations: [...PAGES],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
  ]
})
export class PagesModule { }
